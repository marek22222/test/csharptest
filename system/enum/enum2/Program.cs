﻿// public enum ArrivalStatus { Unknown=-3, Late=-1, OnTime=0, Early=1 };
public enum DicPoziomyLokalizacji { KONTYNENT=0, PANSTWO=1, REGION=2, MIASTO=3, ATRAKCJA=4 }

public class Example
{
   public static void Main()
   {
      for (int i = 0; i < 5; i++)
      {
            DicPoziomyLokalizacji poziom;
            if (Enum.IsDefined(typeof(DicPoziomyLokalizacji), i))
               poziom = (DicPoziomyLokalizacji)i;
            else
               poziom = DicPoziomyLokalizacji.ATRAKCJA;
            Console.WriteLine("Konwersja {0:N0} do {1}", i, poziom.ToString());
      }
   }
}
// The example displays the following output:
//       Converted -3 to Unknown
//       Converted -1 to Late
//       Converted 0 to OnTime
//       Converted 1 to Early
//       Converted 5 to Unknown
//       Converted 2,147,483,647 to Unknown