﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace linq2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("3 listy!");
            List<string> list1 = new List<string> { "A", "B", "C" };
            List<string> list2 = new List<string> { "=", "<", ">" };
            List<string> list3 = new List<string> { "1", "2", "3" };

            string result = string.Join(" AND ",
                list1.Zip(list2, (l1, l2) => l1 + l2).Zip(list3, (l2, l3) => l2 + l3));
            Console.WriteLine($"result: {result}");

            result = string.Join(" AND ", list1.Select((e1, idx) => $"{e1}{list2[idx]}{list3[idx]}"));
            Console.WriteLine($"result: {result}");
        }

    }
}
