using Newtonsoft.Json;
using Test01.Lokalizacje.Models;

namespace Test01.Lokalizacje
{
    public class Lokalizacje
    {

        private int? PobierzIdRodzica(int typRodzica, string? kluczRodzica, int? idRodzica)
        {   
            int? id = null;
            if (kluczRodzica == null)
                return id;

            if (typRodzica == kluczRodzica.Split(':').Count())
                id = idRodzica;

            return id;
        }

        public List<ModelWidokLokalizacjeRekurencyjnie> PobierzWidokLokalizacjeRekurencyjnie()
        {
            List<FiltryLokalizacje> filtry;
            using (StreamReader r = new StreamReader("Lokalizacje\\dane.json"))
            {
                string json = r.ReadToEnd();
                filtry = JsonConvert.DeserializeObject<List<FiltryLokalizacje>>(json);
            }
            Console.WriteLine("filtry: liczba: {0:N0}, pierwszy: {1}", filtry.Count, filtry[0]);


            var lista = new List<ModelWidokLokalizacjeRekurencyjnie>();
            int liczbaFiltrow = filtry.Count;
            for (int k = 0; k < liczbaFiltrow; k++)
            {
                FiltryLokalizacje filtr = filtry[k];

                string[] kluczLista = (filtr.Klucz).Split(':');
                for (int l = 0; l < kluczLista.Length; l++)
                    if (kluczLista[l] == "-")
                        kluczLista[l] = "";

                var model = new ModelWidokLokalizacjeRekurencyjnie();
                int dlKlucza = kluczLista.Length;
                int typLokal = kluczLista.Length;
                int typRodz  = typLokal - 1;

                Console.WriteLine($"Typ: {(DicTypyLokalizacji)typLokal} - {filtr.Klucz}");

                model.LokazlizacjaId= filtr.Id;
                model.Klucz         = filtr.Klucz;
                model.Kontynent = dlKlucza > 0 ? kluczLista[0] : null;
                model.Panstwo   = dlKlucza > 1 ? kluczLista[1] : null;
                model.Region    = dlKlucza > 2 ? kluczLista[2] : null;
                model.Miasto    = dlKlucza > 3 ? kluczLista[3] : null;
                model.Atrakcja  = dlKlucza > 4 ? kluczLista[4] : null;
                model.TypLokalizacji = (DicTypyLokalizacji)typLokal;
                model.PrzewodnikTypyAtrakcjiId = null;

                model.KontynentId   = typLokal == 1 ? filtr.Id : PobierzIdRodzica(1, filtr.RodzicKlucz, filtr.RodzicId);
                model.PanstwoId     = typLokal == 2 ? filtr.Id : PobierzIdRodzica(2, filtr.RodzicKlucz, filtr.RodzicId);
                model.RegionId      = typLokal == 3 ? filtr.Id : PobierzIdRodzica(3, filtr.RodzicKlucz, filtr.RodzicId);
                model.MiastoId      = typLokal == 4 ? filtr.Id : PobierzIdRodzica(4, filtr.RodzicKlucz, filtr.RodzicId);
                model.AtrakcjaId    = typLokal == 5 ? filtr.Id : null;

                if (typLokal == 2)
                    typLokal = typLokal;

                lista.Add(model);
            }

 
            // sort i aktualizacje Id
            lista = lista.OrderBy(k => k.Kontynent).ThenBy(p => p.Panstwo).ThenBy(p => p.Region).ThenBy(p => p.Miasto).ThenBy(p => p.Atrakcja).ToList();

            // update
            // (from p in Context.person_account_portfolio 
            // where p.person_id == personId select p).ToList()
            //                             .ForEach(x => x.is_default = false);

            var listaKon      = (lista.Where(k => k.KontynentId != null && !string.IsNullOrEmpty(k.Kontynent) 
                                && k.TypLokalizacji.Equals(DicTypyLokalizacji.KONTYNENT))).ToList();
            var listaPanstw   = (lista.Where(k => k.PanstwoId != null && !string.IsNullOrEmpty(k.Panstwo) 
                                && k.TypLokalizacji.Equals(DicTypyLokalizacji.PANSTWO))).ToList();
            var listaRegionow = (lista.Where(k => k.RegionId != null && !string.IsNullOrEmpty(k.Region) 
                                && k.TypLokalizacji.Equals(DicTypyLokalizacji.REGION))).ToList();
            var listaMiast    = (lista.Where(k => k.MiastoId != null && !string.IsNullOrEmpty(k.Miasto) 
                                && k.TypLokalizacji.Equals(DicTypyLokalizacji.MIASTO))).ToList();
            foreach (var poz in lista)
            {
                if (poz.KontynentId == null && !string.IsNullOrEmpty(poz.Kontynent))
                {
                    poz.KontynentId = listaKon.Where(c=>c.Kontynent == poz.Kontynent).Select(s=>s.KontynentId).FirstOrDefault();
                }
                if (poz.PanstwoId == null && !string.IsNullOrEmpty(poz.Panstwo))
                {
                    poz.PanstwoId = listaPanstw.Where(c=>c.Panstwo == poz.Panstwo).Select(s=>s.PanstwoId).FirstOrDefault();
                }
                if (poz.RegionId == null && !string.IsNullOrEmpty(poz.Region))
                {
                    poz.RegionId = listaRegionow.Where(c=>c.Region == poz.Region).Select(s=>s.RegionId).FirstOrDefault();
                }
                if (poz.MiastoId == null && !string.IsNullOrEmpty(poz.Miasto))
                {
                    poz.MiastoId = listaMiast.Where(c=>c.Miasto == poz.Miasto).Select(s=>s.MiastoId).FirstOrDefault();
                }

                Console.WriteLine($"Poz: {poz.KontynentId}:{poz.PanstwoId}:{poz.RegionId}:{poz.MiastoId}:{poz.AtrakcjaId} - {poz.Klucz}");
                // Console.WriteLine($"      {poz.KontynentId}:{poz.PanstwoId}:{poz.RegionId}:{poz.MiastoId}:{poz.AtrakcjaId}");
            }
 
            Console.WriteLine($"Liczba danych: {lista.Count}");
            return lista;
        }

    }
}