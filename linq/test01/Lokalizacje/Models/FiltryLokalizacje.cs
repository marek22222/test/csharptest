namespace Test01.Lokalizacje.Models
{
    public class FiltryLokalizacje
    {
        public int Id { get; set; }
        public string? Klucz { get; set; }
        public string? Nazwa { get; set; }
        public int? RodzicId { get; set; }
        public string? RodzicKlucz { get; set; }
        public int NodeLevel()
        {
            return Klucz.Count(c => c == ':')+1;
        }
    }
}
