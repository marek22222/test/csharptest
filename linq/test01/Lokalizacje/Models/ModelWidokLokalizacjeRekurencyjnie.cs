namespace Test01.Lokalizacje.Models
{
	public class ModelWidokLokalizacjeRekurencyjnie
	{
		public int LokazlizacjaId { get; set; }
        public string? Klucz { get; set; }

		public int? KontynentId { get; set; }
        public string? Kontynent { get; set; }

		public int? RegionId { get; set; }
		public string? Region { get; set; }

		public int? PanstwoId { get; set; }
		public string? Panstwo { get; set; }

		public int? MiastoId { get; set; }
		public string? Miasto { get; set; }

		public int? AtrakcjaId { get; set; }
		public string? Atrakcja { get; set; }

		public DicTypyLokalizacji TypLokalizacji { get; set; }

        public int? PrzewodnikTypyAtrakcjiId { get; set; }
	}

    public enum DicTypyLokalizacji
    {
        Niekreslony = 0,

        KONTYNENT = 1,
        PANSTWO = 2,
        REGION = 3,
        MIASTO = 4,
        ATRAKCJA = 5
    }

    public enum DicPoziomyLokalizacji
    {
        KONTYNENT = 0,
        PANSTWO = 1,
        REGION = 2,
        MIASTO = 3,
        ATRAKCJA = 4
    }

}